class PatientController < ApplicationController
  def show
    @patient = Patient.find(params['id'])
  end

  def edit
    @patient = Patient.find(params['id'])
  end

  def create
    @patient = Patient.new
  end

  def destroy
    @patient = Patient.find(params['id'])
    render 'warning'
  end

  def delete
    @patient = Patient.find(params['id'])
    @patient.destroy

    redirect_to :controller => 'home', :action => 'index'
  end

  def save
    @patient = Patient.new
    @patient.first_name = params['patient']['first_name']
    @patient.last_name = params['patient']['last_name']
    @patient.middle_name = params['patient']['middle_name']
    @patient.weight = params['patient']['weight']
    @patient.height = params['patient']['height']
    @patient.MRN = params['patient']['MRN']

    if @patient.valid?
      @patient.save
      redirect_to :action => 'show', :id => @patient.id
    else
      render 'create'
    end
  end

  def update
    if params['id']
      @patient = Patient.find(params['id'])
      @patient.first_name = params['patient']['first_name']
      @patient.last_name = params['patient']['last_name']
      @patient.middle_name = params['patient']['middle_name']
      @patient.weight = params['patient']['weight']
      @patient.height = params['patient']['height']
      @patient.MRN = params['patient']['MRN']

      # can't catch exceptions for some reason, let's move to if/else
      # begin
      #   @patient.save!
      #   redirect_to :controller => 'home', :action => 'index'
      # rescue => e
      #   puts e.record.errors.to_s
      #   render edit
      # end

      if @patient.valid?
        @patient.save
        redirect_to :action => 'show', :id => params['id']
      else
        render 'edit'
      end
    end
  end
end
