class EncountersController < ApplicationController

  def create
    @enc = Encounter.new
    @patient = Patient.find(params['patient_id'])
  end

  def show
    @enc = Encounter.find(params['id'])
  end

  def edit
    @enc = Encounter.find(params['id'])
  end

  def save
    @patient = Patient.find(params['patient_id'])
    @enc = Encounter.new

    @enc.patient = @patient
    @enc.visit_number = params['encounter']['visit_number']
    @enc.admitted_at = params['encounter']['admitted_at']
    @enc.discharged_at = params['encounter']['discharged_at']
    @enc.location = params['encounter']['location']
    @enc.room = params['encounter']['room']
    @enc.bed = params['encounter']['bed']

    # if the model values are valid, proceed towards saving the model.
    # once saved, redirect to patient page.
    # else show the errors on the form.
    if @enc.valid?
      @enc.save
      redirect_to :controller => 'patient', :action => 'show', :id => @enc.patient.id
    else
      render 'create'
    end
  end

  def update

    # Haven't read about private or internal functions yet, so duplicating the code.
    @enc = Encounter.find{params['id']}

    @enc.visit_number = params['encounter']['visit_number']
    @enc.admitted_at = params['encounter']['admitted_at']
    @enc.discharged_at = params['encounter']['discharged_at']
    @enc.location = params['encounter']['location']
    @enc.room = params['encounter']['room']
    @enc.bed = params['encounter']['bed']

    # if the model values are valid, proceed towards saving the model.
    # once saved, redirect to patient page.
    # else show the errors on the form.
    if @enc.valid?
      @enc.save
      redirect_to :controller => 'patient', :action => 'show', :id => @enc.patient.id
    else
      render 'edit'
    end
  end

  def destroy
    @enc = Encounter.find(params['id'])
    render 'warning'
  end

  def delete
    @enc = Encounter.find(params['id'])
    patient_id = @enc.patient.id
    @enc.delete

    redirect_to :controller => 'patient', :action => 'show', :id => patient_id
  end
end
