class Patient < ApplicationRecord
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :weight, presence: true
  validates :height, presence: true
  validates :MRN, presence: true

  has_many :encounters, :dependent => :destroy
end
