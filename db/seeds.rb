# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Patient.create([{
    first_name: 'Jaye',
    middle_name: 'R\'p',
    last_name: 'Massinger',
    weight: 55.68,
    height: 146.18,
    MRN: 'JHx5axtF'
}, {
    first_name: 'Langsdon',
    middle_name: 'Burr',
    last_name: 'Scare',
    weight: 139.17,
    height: 151.01,
    MRN: '6P5V4v'
}, {
    first_name: 'Broderic',
    middle_name: 'E\'uardo',
    last_name: 'Kighly',
    weight: 119.88,
    height: 126.59,
    MRN: 'rR4wPPll'
}, {
    first_name: 'Ariel',
    middle_name: 'Griffy',
    last_name: 'Szymanski',
    weight: 60.9,
    height: 173.83,
    MRN: 'grpaGez3bIeK'
}, {
    first_name: 'Karel',
    middle_name: 'I\'gelbert',
    last_name: 'Terrett',
    weight: 146.74,
    height: 169.91,
    MRN: 'SZ6GOWvkYeIP'
}, {
    first_name: 'Nadiya',
    middle_name: 'Scottie',
    last_name: 'Trenbay',
    weight: 93.04,
    height: 182.64,
    MRN: 'FtHooE2il2Z'
}, {
    first_name: 'Junina',
    middle_name: 'D\'menico',
    last_name: 'Gothup',
    weight: 67.5,
    height: 196.57,
    MRN: '96ZvCR'
}, {
    first_name: 'Linnea',
    middle_name: 'Prinz',
    last_name: 'Weatherdon',
    weight: 96.97,
    height: 171.49,
    MRN: 'RKY5TKW6vxh'
}, {
    first_name: 'Lesya',
    middle_name: 'M\'ckie',
    last_name: 'Lowery',
    weight: 84.73,
    height: 152.09,
    MRN: '61rS2uQg1iXt'
}, {
    first_name: 'Dermot',
    middle_name: 'Lion',
    last_name: 'Fairbourne',
    weight: 97.47,
    height: 179.74,
    MRN: 'FzMx0WtC'
}])

Encounter.create([{
    visit_number: 'IPYOA_1689',
    admitted_at: '2004-10-30 19:07:19',
    discharged_at: '2016-08-23 07:12:30',
    location: 'sagittis dui',
    room: 'C-77',
    bed: 'HC-403',
    patient_id: 1
}, {
    visit_number: 'VSZPC_4043',
    admitted_at: '2014-09-15 14:22:43',
    discharged_at: '2016-12-09 04:26:34',
    location: 'montes',
    room: 'J-93',
    bed: 'XB-639',
    patient_id: 9
}, {
    visit_number: 'CXAWN_4177',
    admitted_at: '2008-11-12 12:15:07',
    discharged_at: nil,
    location: 'volutpat convallis',
    room: 'N-56',
    bed: 'BS-263',
    patient_id: 8
}, {
    visit_number: 'BGYRQ_2447',
    admitted_at: '2001-02-16 06:31:02',
    discharged_at: '2014-09-11 03:01:08',
    location: 'consequat',
    room: 'D-88',
    bed: 'HD-932',
    patient_id: 5
}, {
    visit_number: 'FGEKH_3016',
    admitted_at: '2007-12-04 00:47:23',
    discharged_at: '2016-08-01 06:06:53',
    location: 'consectetuer',
    room: 'Q-37',
    bed: 'IZ-456',
    patient_id: 2
}, {
    visit_number: 'WQSBO_4865',
    admitted_at: '2001-03-16 09:53:16',
    discharged_at: nil,
    location: 'praesent',
    room: 'Z-62',
    bed: 'JN-783',
    patient_id: 9
}, {
    visit_number: 'PCYLQ_5219',
    admitted_at: '2003-02-13 02:13:34',
    discharged_at: nil,
    location: 'non velit',
    room: 'T-10',
    bed: 'PR-821',
    patient_id: 6
}, {
    visit_number: 'WBXDK_1064',
    admitted_at: '2001-08-15 11:41:08',
    discharged_at: nil,
    location: 'aliquet',
    room: 'U-52',
    bed: 'DF-273',
    patient_id: 10
}, {
    visit_number: 'GBHPJ_8032',
    admitted_at: '2001-04-05 02:52:41',
    discharged_at: '2002-07-06 18:02:26',
    location: 'nec',
    room: 'Q-09',
    bed: 'MT-116',
    patient_id: 2
}, {
    visit_number: 'PSKYE_2374',
    admitted_at: '2008-03-24 11:09:04',
    discharged_at: nil,
    location: 'non',
    room: 'N-68',
    bed: 'HG-827',
    patient_id: 2
}, {
    visit_number: 'WPERA_7782',
    admitted_at: '2007-06-19 07:17:36',
    discharged_at: nil,
    location: 'ligula in',
    room: 'L-08',
    bed: 'JF-626',
    patient_id: 8
}, {
    visit_number: 'DLYCB_6832',
    admitted_at: '2003-02-24 02:20:19',
    discharged_at: nil,
    location: 'suscipit a',
    room: 'L-35',
    bed: 'BM-029',
    patient_id: 8
}, {
    visit_number: 'MPCZS_7260',
    admitted_at: '2004-04-21 13:48:33',
    discharged_at: nil,
    location: 'id',
    room: 'I-69',
    bed: 'JW-891',
    patient_id: 7
}, {
    visit_number: 'VLMBQ_1196',
    admitted_at: '2011-06-08 21:57:56',
    discharged_at: '2007-02-17 15:10:37',
    location: 'hac habitasse',
    room: 'O-82',
    bed: 'CT-736',
    patient_id: 8
}, {
    visit_number: 'TFEKN_1072',
    admitted_at: '2016-11-09 05:00:23',
    discharged_at: '2004-11-26 09:33:16',
    location: 'erat',
    room: 'H-69',
    bed: 'HM-483',
    patient_id: 3
}, {
    visit_number: 'EANQR_7711',
    admitted_at: '2013-06-29 11:27:59',
    discharged_at: '2001-09-24 00:41:40',
    location: 'porttitor',
    room: 'B-42',
    bed: 'SB-276',
    patient_id: 4
}, {
    visit_number: 'MYZLU_8023',
    admitted_at: '2011-11-06 23:08:07',
    discharged_at: '2002-08-03 22:32:09',
    location: 'est',
    room: 'J-98',
    bed: 'ED-849',
    patient_id: 10
}, {
    visit_number: 'BOCUE_9993',
    admitted_at: '2013-08-31 10:49:41',
    discharged_at: '2011-01-29 03:22:07',
    location: 'fusce lacus',
    room: 'B-62',
    bed: 'TE-399',
    patient_id: 6
}, {
    visit_number: 'XRAZQ_6373',
    admitted_at: '2012-08-30 05:43:06',
    discharged_at: '2015-11-24 16:35:08',
    location: 'imperdiet nilam',
    room: 'I-54',
    bed: 'HA-940',
    patient_id: 8
}, {
    visit_number: 'ZECUO_8405',
    admitted_at: '2003-02-17 22:25:21',
    discharged_at: '2008-08-01 00:21:25',
    location: 'amet',
    room: 'M-47',
    bed: 'LG-932',
    patient_id: 1
}, {
    visit_number: 'ZOSTC_5604',
    admitted_at: '2004-07-02 00:39:25',
    discharged_at: nil,
    location: 'curabitur convallis',
    room: 'I-84',
    bed: 'JF-832',
    patient_id: 8
}, {
    visit_number: 'LRWGV_3884',
    admitted_at: '2016-07-10 07:32:47',
    discharged_at: '2006-11-23 21:03:33',
    location: 'volutpat',
    room: 'A-13',
    bed: 'GN-476',
    patient_id: 6
}, {
    visit_number: 'VDICB_8253',
    admitted_at: '2003-08-14 05:58:53',
    discharged_at: '2010-01-17 04:03:15',
    location: 'pellentesque',
    room: 'R-50',
    bed: 'NK-570',
    patient_id: 7
}, {
    visit_number: 'KHUFI_0513',
    admitted_at: '2003-11-07 12:41:51',
    discharged_at: nil,
    location: 'posuere metus',
    room: 'T-62',
    bed: 'CK-460',
    patient_id: 4
}, {
    visit_number: 'FHYPX_5998',
    admitted_at: '2001-09-14 10:57:43',
    discharged_at: '2006-01-07 17:08:33',
    location: 'adipiscing elit',
    room: 'J-96',
    bed: 'PX-475',
    patient_id: 3
}, {
    visit_number: 'ZGYWE_7509',
    admitted_at: '2010-10-23 22:17:02',
    discharged_at: '2004-12-29 14:21:26',
    location: 'etiam',
    room: 'Z-91',
    bed: 'TC-413',
    patient_id: 5
}, {
    visit_number: 'JGYNO_7591',
    admitted_at: '2013-03-01 03:17:45',
    discharged_at: nil,
    location: 'eu nibh',
    room: 'Y-84',
    bed: 'OK-064',
    patient_id: 8
}, {
    visit_number: 'LSVQM_2434',
    admitted_at: '2015-03-02 09:12:38',
    discharged_at: nil,
    location: 'viverra dapibus',
    room: 'O-63',
    bed: 'SJ-695',
    patient_id: 6
}, {
    visit_number: 'XWMKS_2264',
    admitted_at: '2002-06-20 05:51:08',
    discharged_at: '2006-11-18 04:19:41',
    location: 'in',
    room: 'X-50',
    bed: 'WS-249',
    patient_id: 1
}, {
    visit_number: 'VETUM_0088',
    admitted_at: '2003-05-30 09:50:02',
    discharged_at: nil,
    location: 'quis justo',
    room: 'Q-33',
    bed: 'MU-884',
    patient_id: 1
}, {
    visit_number: 'HJDRK_0889',
    admitted_at: '2004-09-10 16:21:21',
    discharged_at: nil,
    location: 'morbi non',
    room: 'V-06',
    bed: 'RD-859',
    patient_id: 3
}, {
    visit_number: 'PKGJM_0505',
    admitted_at: '2007-05-17 23:55:33',
    discharged_at: '2002-11-24 11:08:20',
    location: 'arcu',
    room: 'I-17',
    bed: 'BX-582',
    patient_id: 9
}, {
    visit_number: 'SYOMW_2995',
    admitted_at: '2016-05-21 12:30:56',
    discharged_at: '2008-07-17 14:24:39',
    location: 'eget elit',
    room: 'C-48',
    bed: 'VK-395',
    patient_id: 4
}, {
    visit_number: 'WGVSI_9409',
    admitted_at: '2002-03-03 18:02:57',
    discharged_at: nil,
    location: 'auctor',
    room: 'K-57',
    bed: 'RE-073',
    patient_id: 5
}, {
    visit_number: 'JVBAZ_9875',
    admitted_at: '2004-01-25 05:11:13',
    discharged_at: '2008-07-24 13:10:16',
    location: 'accumsan',
    room: 'L-77',
    bed: 'LP-898',
    patient_id: 5
}, {
    visit_number: 'ODYXP_7859',
    admitted_at: '2014-09-04 11:04:28',
    discharged_at: nil,
    location: 'sit amet',
    room: 'P-91',
    bed: 'PS-491',
    patient_id: 6
}, {
    visit_number: 'MFLPU_6782',
    admitted_at: '2002-10-23 10:22:33',
    discharged_at: nil,
    location: 'felis',
    room: 'S-99',
    bed: 'RB-888',
    patient_id: 5
}, {
    visit_number: 'SQWOD_6223',
    admitted_at: '2002-09-29 13:22:50',
    discharged_at: '2004-02-01 11:03:11',
    location: 'montes',
    room: 'C-43',
    bed: 'OH-358',
    patient_id: 6
}, {
    visit_number: 'OILQC_9123',
    admitted_at: '2006-02-24 08:31:30',
    discharged_at: nil,
    location: 'blandit',
    room: 'N-92',
    bed: 'PZ-632',
    patient_id: 7
}, {
    visit_number: 'MOQIE_3031',
    admitted_at: '2009-09-19 04:43:25',
    discharged_at: nil,
    location: 'nunc',
    room: 'S-78',
    bed: 'AN-068',
    patient_id: 1
}, {
    visit_number: 'JMNVE_0153',
    admitted_at: '2007-04-11 06:31:25',
    discharged_at: nil,
    location: 'nunc donec',
    room: 'B-10',
    bed: 'BR-772',
    patient_id: 4
}, {
    visit_number: 'OYSME_0926',
    admitted_at: '2013-01-17 12:56:02',
    discharged_at: nil,
    location: 'ut erat',
    room: 'G-69',
    bed: 'KN-523',
    patient_id: 3
}, {
    visit_number: 'XPZBY_9695',
    admitted_at: '2016-04-11 05:05:07',
    discharged_at: '2005-01-18 11:00:19',
    location: 'dui maecenas',
    room: 'S-92',
    bed: 'ON-334',
    patient_id: 9
}, {
    visit_number: 'MITVR_4889',
    admitted_at: '2006-06-27 07:10:39',
    discharged_at: nil,
    location: 'rutrum ac',
    room: 'S-04',
    bed: 'LX-804',
    patient_id: 2
}, {
    visit_number: 'ZQNTV_6903',
    admitted_at: '2012-10-06 06:22:09',
    discharged_at: nil,
    location: 'ac diam',
    room: 'N-66',
    bed: 'QC-294',
    patient_id: 5
}, {
    visit_number: 'NUTPM_3741',
    admitted_at: '2014-03-14 07:43:30',
    discharged_at: nil,
    location: 'nunc commodo',
    room: 'A-76',
    bed: 'GJ-132',
    patient_id: 7
}, {
    visit_number: 'IAFCZ_9097',
    admitted_at: '2015-12-27 22:26:36',
    discharged_at: nil,
    location: 'proin interdum',
    room: 'X-71',
    bed: 'BU-693',
    patient_id: 10
}, {
    visit_number: 'NDEXK_8600',
    admitted_at: '2001-07-25 07:28:51',
    discharged_at: nil,
    location: 'sit amet',
    room: 'K-03',
    bed: 'MI-996',
    patient_id: 6
}, {
    visit_number: 'JFBHI_5240',
    admitted_at: '2003-03-31 23:19:07',
    discharged_at: '2002-02-16 09:38:33',
    location: 'erat',
    room: 'R-17',
    bed: 'JO-358',
    patient_id: 7
}, {
    visit_number: 'QIJYR_9502',
    admitted_at: '2009-03-13 05:57:40',
    discharged_at: nil,
    location: 'turpis enim',
    room: 'R-03',
    bed: 'YA-808',
    patient_id: 7
}, {
    visit_number: 'HIUFV_3182',
    admitted_at: '2005-02-16 23:20:46',
    discharged_at: '2016-10-16 23:08:36',
    location: 'venenatis non',
    room: 'S-17',
    bed: 'XE-307',
    patient_id: 10
}, {
    visit_number: 'RJVQF_4121',
    admitted_at: '2014-05-07 10:26:40',
    discharged_at: '2007-07-29 20:21:56',
    location: 'erat',
    room: 'F-82',
    bed: 'XS-257',
    patient_id: 7
}, {
    visit_number: 'ZKOMJ_3937',
    admitted_at: '2016-10-21 08:36:03',
    discharged_at: '2002-03-22 17:51:07',
    location: 'sagittis sapien',
    room: 'D-90',
    bed: 'KY-108',
    patient_id: 8
}, {
    visit_number: 'FORZQ_3973',
    admitted_at: '2013-06-01 18:21:31',
    discharged_at: '2008-08-05 12:26:57',
    location: 'habitasse platea',
    room: 'Z-60',
    bed: 'QS-913',
    patient_id: 7
}, {
    visit_number: 'KUPBS_3064',
    admitted_at: '2015-11-10 10:36:35',
    discharged_at: '2017-05-19 06:28:14',
    location: 'orci',
    room: 'X-95',
    bed: 'WU-971',
    patient_id: 3
}, {
    visit_number: 'RJBKC_0092',
    admitted_at: '2004-06-20 12:40:26',
    discharged_at: '2002-04-23 18:22:45',
    location: 'luctus',
    room: 'K-37',
    bed: 'IX-781',
    patient_id: 8
}, {
    visit_number: 'VNEIC_0550',
    admitted_at: '2007-09-16 22:09:17',
    discharged_at: '2003-09-20 06:16:03',
    location: 'sit',
    room: 'U-97',
    bed: 'WE-985',
    patient_id: 9
}, {
    visit_number: 'TELQK_7676',
    admitted_at: '2007-05-20 11:55:52',
    discharged_at: nil,
    location: 'sapien',
    room: 'K-55',
    bed: 'UO-849',
    patient_id: 2
}, {
    visit_number: 'ZNYTQ_9409',
    admitted_at: '2010-01-17 05:04:45',
    discharged_at: nil,
    location: 'erat',
    room: 'V-01',
    bed: 'GS-760',
    patient_id: 9
}, {
    visit_number: 'DNERH_4373',
    admitted_at: '2005-04-08 19:41:04',
    discharged_at: '2012-05-25 04:42:30',
    location: 'ridiculus',
    room: 'B-66',
    bed: 'HN-583',
    patient_id: 4
}, {
    visit_number: 'RAUDQ_2880',
    admitted_at: '2007-03-01 23:45:44',
    discharged_at: nil,
    location: 'ipsum primis',
    room: 'Z-13',
    bed: 'JU-192',
    patient_id: 6
}, {
    visit_number: 'JWRVC_2617',
    admitted_at: '2001-09-18 06:31:42',
    discharged_at: nil,
    location: 'pulvinar',
    room: 'K-92',
    bed: 'HW-970',
    patient_id: 8
}, {
    visit_number: 'PJCDZ_5045',
    admitted_at: '2011-09-16 17:55:13',
    discharged_at: nil,
    location: 'leo',
    room: 'L-15',
    bed: 'LS-191',
    patient_id: 2
}, {
    visit_number: 'TNQHK_5139',
    admitted_at: '2014-09-11 23:19:29',
    discharged_at: '2009-06-14 16:26:03',
    location: 'purus',
    room: 'K-37',
    bed: 'JU-359',
    patient_id: 3
}, {
    visit_number: 'WMLEI_0865',
    admitted_at: '2012-09-17 05:33:32',
    discharged_at: '2010-07-09 20:47:59',
    location: 'sapien a',
    room: 'J-74',
    bed: 'DX-687',
    patient_id: 4
}, {
    visit_number: 'HNQBG_8765',
    admitted_at: '2001-05-07 23:01:41',
    discharged_at: nil,
    location: 'pede',
    room: 'V-54',
    bed: 'DC-565',
    patient_id: 4
}, {
    visit_number: 'JSEIQ_1081',
    admitted_at: '2005-08-14 07:40:25',
    discharged_at: nil,
    location: 'quam',
    room: 'N-76',
    bed: 'WU-386',
    patient_id: 6
}, {
    visit_number: 'MTAJZ_1466',
    admitted_at: '2015-12-20 22:12:22',
    discharged_at: nil,
    location: 'sed',
    room: 'Z-14',
    bed: 'RN-021',
    patient_id: 9
}, {
    visit_number: 'KNCOS_6810',
    admitted_at: '2008-10-08 14:37:31',
    discharged_at: '2013-05-11 20:48:58',
    location: 'tempus',
    room: 'X-75',
    bed: 'OC-830',
    patient_id: 5
}, {
    visit_number: 'OCVMX_9069',
    admitted_at: '2004-12-29 23:51:34',
    discharged_at: nil,
    location: 'sapien varius',
    room: 'M-65',
    bed: 'KD-693',
    patient_id: 10
}, {
    visit_number: 'AICSB_8884',
    admitted_at: '2010-07-12 07:09:11',
    discharged_at: nil,
    location: 'sodales',
    room: 'B-94',
    bed: 'MG-411',
    patient_id: 6
}, {
    visit_number: 'BKSTU_3286',
    admitted_at: '2002-04-08 09:08:20',
    discharged_at: nil,
    location: 'potenti',
    room: 'S-69',
    bed: 'OQ-298',
    patient_id: 4
}, {
    visit_number: 'KVMFE_2235',
    admitted_at: '2002-02-05 08:10:15',
    discharged_at: nil,
    location: 'felis sed',
    room: 'M-46',
    bed: 'WP-553',
    patient_id: 10
}, {
    visit_number: 'OYFEL_5224',
    admitted_at: '2015-07-27 02:57:03',
    discharged_at: '2016-07-03 14:55:28',
    location: 'auctor',
    room: 'L-39',
    bed: 'JX-471',
    patient_id: 5
}, {
    visit_number: 'NWYUQ_6236',
    admitted_at: '2011-05-11 17:42:16',
    discharged_at: '2007-07-08 21:12:38',
    location: 'eget',
    room: 'W-51',
    bed: 'OG-867',
    patient_id: 8
}, {
    visit_number: 'CZQOP_8508',
    admitted_at: '2005-11-04 09:12:33',
    discharged_at: '2014-05-14 15:00:32',
    location: 'pede justo',
    room: 'Q-93',
    bed: 'YB-653',
    patient_id: 7
}, {
    visit_number: 'IVWUD_6938',
    admitted_at: '2012-07-21 01:26:08',
    discharged_at: nil,
    location: 'sit',
    room: 'J-57',
    bed: 'AR-553',
    patient_id: 3
}, {
    visit_number: 'BSGJR_5755',
    admitted_at: '2014-09-20 22:46:06',
    discharged_at: nil,
    location: 'posuere',
    room: 'Q-14',
    bed: 'UY-664',
    patient_id: 10
}, {
    visit_number: 'JQBKS_1346',
    admitted_at: '2009-08-27 07:12:30',
    discharged_at: '2016-03-06 16:00:45',
    location: 'sit',
    room: 'N-57',
    bed: 'ES-825',
    patient_id: 6
}, {
    visit_number: 'DYZTW_2701',
    admitted_at: '2003-03-07 12:31:42',
    discharged_at: '2013-11-05 00:04:34',
    location: 'justo',
    room: 'T-05',
    bed: 'QW-659',
    patient_id: 2
}, {
    visit_number: 'YRSNW_3186',
    admitted_at: '2012-12-24 21:34:24',
    discharged_at: nil,
    location: 'ridiculus mus',
    room: 'K-10',
    bed: 'XW-670',
    patient_id: 10
}, {
    visit_number: 'LBVFU_9457',
    admitted_at: '2002-11-09 21:14:42',
    discharged_at: nil,
    location: 'aliquam',
    room: 'F-70',
    bed: 'GP-846',
    patient_id: 4
}, {
    visit_number: 'NDBKR_3970',
    admitted_at: '2013-09-23 14:06:04',
    discharged_at: nil,
    location: 'diam',
    room: 'Y-20',
    bed: 'GC-248',
    patient_id: 5
}, {
    visit_number: 'CQIVG_6732',
    admitted_at: '2004-06-12 12:14:42',
    discharged_at: nil,
    location: 'cubilia',
    room: 'X-08',
    bed: 'EA-991',
    patient_id: 9
}, {
    visit_number: 'YBOWX_2371',
    admitted_at: '2008-06-29 15:16:17',
    discharged_at: nil,
    location: 'libero ut',
    room: 'L-36',
    bed: 'ZX-841',
    patient_id: 9
}, {
    visit_number: 'HUQWB_8976',
    admitted_at: '2010-10-21 03:06:21',
    discharged_at: '2007-08-17 13:20:40',
    location: 'in',
    room: 'V-33',
    bed: 'CQ-003',
    patient_id: 2
}, {
    visit_number: 'QORHA_9223',
    admitted_at: '2013-09-07 23:55:49',
    discharged_at: nil,
    location: 'scelerisque mauris',
    room: 'C-21',
    bed: 'KO-412',
    patient_id: 1
}, {
    visit_number: 'WFIGO_4040',
    admitted_at: '2014-02-01 05:43:34',
    discharged_at: '2006-05-13 23:25:27',
    location: 'laoreet ut',
    room: 'K-00',
    bed: 'ME-054',
    patient_id: 8
}, {
    visit_number: 'LUIYN_9094',
    admitted_at: '2007-05-22 12:16:11',
    discharged_at: nil,
    location: 'elementum',
    room: 'Q-61',
    bed: 'FI-053',
    patient_id: 9
}, {
    visit_number: 'TSLHO_7346',
    admitted_at: '2013-01-27 16:00:55',
    discharged_at: '2006-05-18 16:06:15',
    location: 'tortor',
    room: 'L-36',
    bed: 'RO-203',
    patient_id: 10
}, {
    visit_number: 'EMBON_7770',
    admitted_at: '2006-02-25 14:51:32',
    discharged_at: '2012-11-29 11:31:12',
    location: 'libero non',
    room: 'B-06',
    bed: 'XJ-716',
    patient_id: 3
}, {
    visit_number: 'TSGFA_1805',
    admitted_at: '2006-06-08 18:01:42',
    discharged_at: '2001-05-06 23:04:33',
    location: 'duis faucibus',
    room: 'I-26',
    bed: 'BW-048',
    patient_id: 7
}, {
    visit_number: 'EJMLO_6598',
    admitted_at: '2005-08-22 07:57:15',
    discharged_at: nil,
    location: 'nilam orci',
    room: 'D-12',
    bed: 'PE-825',
    patient_id: 9
}, {
    visit_number: 'NOGDX_1128',
    admitted_at: '2014-07-02 03:11:01',
    discharged_at: '2016-01-23 03:01:05',
    location: 'lacinia erat',
    room: 'H-68',
    bed: 'ZI-542',
    patient_id: 3
}, {
    visit_number: 'URXDV_7181',
    admitted_at: '2015-02-15 02:59:59',
    discharged_at: nil,
    location: 'morbi vestibulum',
    room: 'X-41',
    bed: 'KZ-087',
    patient_id: 10
}, {
    visit_number: 'WVTLE_5363',
    admitted_at: '2014-01-14 12:48:00',
    discharged_at: nil,
    location: 'amet nunc',
    room: 'Q-88',
    bed: 'RA-595',
    patient_id: 6
}, {
    visit_number: 'MTLPQ_6048',
    admitted_at: '2003-03-18 10:16:54',
    discharged_at: '2013-04-27 19:22:33',
    location: 'posuere cubilia',
    room: 'H-61',
    bed: 'VL-035',
    patient_id: 10
}, {
    visit_number: 'FGBTW_5713',
    admitted_at: '2006-01-01 15:03:43',
    discharged_at: nil,
    location: 'nibh',
    room: 'R-51',
    bed: 'KC-899',
    patient_id: 8
}, {
    visit_number: 'YXFLK_6556',
    admitted_at: '2007-12-17 10:29:44',
    discharged_at: nil,
    location: 'arcu sed',
    room: 'W-02',
    bed: 'OQ-673',
    patient_id: 5
}, {
    visit_number: 'CRUHE_2881',
    admitted_at: '2014-08-23 13:28:15',
    discharged_at: '2001-04-28 00:31:06',
    location: 'sit',
    room: 'Q-04',
    bed: 'WM-079',
    patient_id: 6
}])