class CreateEncounters < ActiveRecord::Migration[5.1]
  def change
    create_table :encounters do |t|
      t.column :visit_number, :string, :null => false
      t.column :admitted_at, :datetime, :null => false
      t.column :discharged_at, :datetime
      t.column :location, :string
      t.column :room, :string
      t.column :bed, :string
      t.timestamps
    end
  end
end
