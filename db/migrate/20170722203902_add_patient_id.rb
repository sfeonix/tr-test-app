class AddPatientId < ActiveRecord::Migration[5.1]
  def change
    add_column :encounters, :patient_id, :bigint, :null => false
  end
end
