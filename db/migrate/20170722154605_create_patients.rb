class CreatePatients < ActiveRecord::Migration[5.1]
  def change
    create_table :patients do |t|
      t.column :first_name, :string, :null => false, :limit => 50
      t.column :middle_name, :string, :null => false, :limit => 50
      t.column :last_name, :string, :null => false, :limit => 50
      t.column :weight, :float, :precision => 2, :null => false
      t.column :height, :float, :precision => 2, :null => false
      t.column :MRN, :string, :null => false
      t.timestamps
    end
  end
end
