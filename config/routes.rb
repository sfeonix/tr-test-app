Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'patient/show/:id' ,to: 'patient#show'
  get 'patient/edit/:id' ,to: 'patient#edit'
  get 'patient/create' ,to: 'patient#create'
  get 'patient/destroy/:id' ,to: 'patient#destroy'

  delete 'patient/delete/:id' ,to: 'patient#delete'
  post 'patient/save' ,to: 'patient#save'
  patch 'patient/update/:id' ,to: 'patient#update'


  get 'encounter/:patient_id/create' ,to: 'encounters#create'
  get 'encounter/show/:id' ,to: 'encounters#show'
  get 'encounter/edit/:id' ,to: 'encounters#edit'
  get 'encounter/destroy/:id' ,to: 'encounters#destroy'

  patch 'encounter/update/:id' ,to: 'encounters#update'
  post 'encounter/:patient_id/save' ,to: 'encounters#save'
  delete 'encounter/delete/:id' ,to: 'encounters#delete'


  root 'home#index'
end
