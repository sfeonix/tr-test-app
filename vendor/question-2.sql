/*
 * Q1. All of the patients that their last_name contains the word “mit”
 */
SELECT * FROM patients WHERE last_name like '%mit%';

/* Q2. All of the patients have been discharged from the system. That is, all of the patient’s
 *     encounters have a value in the discharged_at column
 * Solution .a
 * If we need to see what patients are currently discharged as in NO pending empty discharge date.
 */
SELECT *
FROM patients AS p1
WHERE p1.id NOT IN (
    SELECT p.id
    FROM patients as p
    JOIN encounters as e ON (e.patient_id = p.id)
    WHERE e.discharged_at IS NULL
);

/* Q2. All of the patients have been discharged from the system. That is, all of the patient’s
 *     encounters have a value in the discharged_at column
 * Solution .b
 * Alternate - sort by latest admission date and check if there is discharge date against it
 */
SELECT p1.*, e1.*
FROM patients AS p1
JOIN encounters AS e1 ON e1.patient_id = p1.id
INNER JOIN (
    SELECT p.*, max(e.admitted_at) as cDate
    FROM patients as p
    JOIN encounters as e
    ON (e.patient_id = p.id)
    GROUP BY p.id
) AS r
ON r.cDate = e1.`admitted_at`
WHERE `discharged_at` IS NOT NULL;

/*
 * Q3. All of the patient that have been admitted in between July 5th, 2014 and August 19,
2014
 */
SELECT p.*
FROM patients AS p
JOIN encounters AS e
ON (e.patient_id = p.id)
WHERE e.admitted_at BETWEEN '2014-07-04 23:59:59' AND '2014-08-19 23:59:59'
GROUP BY p.id;